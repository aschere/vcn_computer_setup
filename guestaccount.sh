#! /bin/bash

#changes a variable in this accounts /users file to disable the account
sudo sed -i s/'SystemAccount=false'/'SystemAccount=true'/ /var/lib/AccountsService/users/vcn

#setup turn off the screen never
 sudo sed -i '/# disable screen locking (MATE)/ i gsettings set org.gnome.desktop.session idle-delay 0' /usr/lib/lightdm/guest-session-auto.sh

#stops ubuntu from prompting user to upgrade to newest version of ubuntu
sudo sed -i 's/=lts/=never/g' /etc/update-manager/release-upgrades

#edits printer settings to set a limit of 10 pages per minute
sudo lpadmin -p 'MFC9970CDW' -o job-quota-period=60 -o job-page-limit=10

#adds a popup to the bc211 website that opens at startup

#Alert user when about to close
(crontab -l 2>/dev/null; echo '45 15 * * * export DISPLAY=:0 && zenity --warning --text="Vancouver Community Network is about to close in 15 minutes. Please save your work."') | crontab -
